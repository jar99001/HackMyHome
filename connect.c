#include <mysql.h>
#include <pigpio.h>
#include <stdio.h>
#include <unistd.h>

#define SOVRUM_PAPPA	1
#define YTTERDORR	2
#define BALKONG		3
#define SOVRUM_BARN	4

void addToDb(MYSQL *db, int state, int doorId) {
    char s[100];
    sprintf(s,"insert into doorevent (state,doorId) values(%d,%d)",state,doorId);

    if(mysql_query(db, s)==0) {
	printf("Added new value to table doorevent\n");
    } else {
	printf("Failed to add new value to table doorevent\n");
    }


}


int main(int argc, char **argv) {
    MYSQL *database;

    // Init GPIO
   if (gpioInitialise() < 0) {
   // pigpio initialisation failed.
    printf("GPIO init failed\n");
    return 0;
    } else {
   // pigpio initialised okay.
    printf("GPIO init successful\n");
    }

    if(gpioSetMode(23, PI_INPUT)==0) {
    printf("Successfully set input mode gpio nr 23\n");
    } else {
    printf("Failed to set input mode gpio nr 23\n");
    }

    // Init database
    database = mysql_init(NULL);
    if(database==NULL)  {
	printf("Error init mysql_init()\n");
	return 0;
    } else {
	printf("Mysql init successful\n");
    }
    if(mysql_real_connect(database,"127.0.0.1","lgh","r29osary","lgh",0,NULL,0)==NULL) {
	printf("Connection failed\n");
	printf("Error status: %s\n",mysql_error(database));
    } else {
	printf("Connection successfull\n");
    }

    int state;
    state = gpioRead(23);

    if(state==PI_BAD_GPIO) {
	printf("Error reading gpio nr 23");
	return 0;
    } else {
	printf("Value from gpio 23: %d\n",state);
    }

    int value;
    while(1) {
    value = gpioRead(23);
    if(value==PI_BAD_GPIO) {
	printf("Error reading gpio nr 23");
    } else {
        if(value==state) {
	    // Values have not changed
        } else {
	    printf("Value from gpio 23 has changed: %d\n",value);
	    addToDb(database, value, SOVRUM_PAPPA);
	    state = value;
        }
    }
	usleep(100000);
    }


    gpioTerminate();
    mysql_close(database);
    return 0;





}